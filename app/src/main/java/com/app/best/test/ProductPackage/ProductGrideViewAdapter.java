package com.app.best.test.ProductPackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.app.best.test.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductGrideViewAdapter extends BaseAdapter {
    public ProductGrideViewAdapter(Context context) {
        this.context = context;
    }

    Context context;
    List<ProductPojoClass> list;
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.gride_product,viewGroup,false);

        Picasso.get().load("https://kasbokarnews.ir/wp-content/uploads/2018/06/20180614102203.jpg").into((ImageView)view1.findViewById(R.id.img_grid_product));
        return view1;
    }
}
