package com.app.best.test;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Adapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private TextView txtDay, txtHour, txtMinute, txtSecond;
    private TextView tvEventStart;
    private Handler handler;
    private Runnable runnable;

    String[] img = {"https://kasbokarnews.ir/wp-content/uploads/2018/06/20180614102203.jpg","http://alavi.ir/ab/wp-content/uploads/sites/7/2016/08/006.jpg"};
    CarouselView carouselView;
    private Button btnPlus;
    private Button btnMines;
    private TextView txtCountBuy;
    private Button btnBuyProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        setClick();
        setCarsoulView();
//        RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);

        countDownStart();

    }

    private void setClick() {
        btnPlus.setOnClickListener(this);
        btnMines.setOnClickListener(this);
        btnBuyProduct.setOnClickListener(this);
    }

    private void bind() {
        txtCountBuy = (TextView) findViewById(R.id.txt_count_buy);
        expListView = (ExpandableListView) findViewById(R.id.Expandablelist);
        txtDay = (TextView) findViewById(R.id.txtDay);
        txtHour = (TextView) findViewById(R.id.txtHour);
        txtMinute = (TextView) findViewById(R.id.txtMinute);
        txtSecond = (TextView) findViewById(R.id.txtSecond);
        btnPlus = (Button)findViewById(R.id.btn_plus);
        btnMines =(Button) findViewById(R.id.btn_mines);
        btnBuyProduct = (Button) findViewById(R.id.btn_buy_product);
        // preparing list data
        prepareListData();

        listAdapter = new Adapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);


        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                final ImageView imageView = view.findViewById(R.id.arrow_group_list);
                //Toast.makeText(MainActivity.this, "aaaa", Toast.LENGTH_SHORT).show();
               TextView click =  view.findViewById(R.id.get_click);
                if (click.getText().equals("0")){
                    click.setText("1");
                    Animation animOpen = AnimationUtils.loadAnimation(MainActivity.this,R.anim.list_arrow_animation);
                    imageView.setAnimation( animOpen);
                    animOpen.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                            imageView.setRotation(270);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }else {


                    Animation animClose = AnimationUtils.loadAnimation(MainActivity.this,R.anim.close_animation);
                    imageView.setAnimation( animClose);
                    animClose.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                            imageView.setRotation(0);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    click.setText("0");
                }

                return false;
            }
        });
    }

    private void setCarsoulView() {
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(img.length);

        carouselView.setImageListener(imageListener);
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Picasso.get().load(img[position]).fit().centerCrop().into(imageView);
        }
    };



    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("نمایش ویژگی های آیتیک");
        listDataHeader.add("نمایش شرایط استفاده از آیتیک");

        // Adding child data
        List<String> films = new ArrayList<String>();
        films.add("سربداران");
        films.add("خواب و بیدار");
        films.add("دزد و پلیس");


        List<String> Directors = new ArrayList<String>();
        Directors.add("ابراهیم حاتمی کیا");
        Directors.add("مهران مدیری");
        Directors.add("مهران غفوریان");




        listDataChild.put(listDataHeader.get(0), films); // Header, Child data
        listDataChild.put(listDataHeader.get(1), Directors);

    }

    public void countDownStart() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd");
                    // Please here set your event date//YYYY-MM-DD
                    Date futureDate = dateFormat.parse("2019-5-30");
                    Date currentDate = new Date();
                    if (!currentDate.after(futureDate)) {
                        long diff = futureDate.getTime()
                                - currentDate.getTime();
                        long days = diff / (24 * 60 * 60 * 1000);
                        diff -= days * (24 * 60 * 60 * 1000);
                        long hours = diff / (60 * 60 * 1000);
                        diff -= hours * (60 * 60 * 1000);
                        long minutes = diff / (60 * 1000);
                        diff -= minutes * (60 * 1000);
                        long seconds = diff / 1000;
                        txtDay.setText("" + String.format("%02d", days));
                        txtHour.setText("" + String.format("%02d", hours));
                        txtMinute.setText(""
                                + String.format("%02d", minutes));
                        txtSecond.setText(""
                                + String.format("%02d", seconds));
                    } else {
                        tvEventStart.setVisibility(View.VISIBLE);
                        tvEventStart.setText("The event started!");
                        textViewGone();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1 * 1000);
    }

    public void textViewGone() {
        findViewById(R.id.LinearLayout1).setVisibility(View.GONE);
        findViewById(R.id.LinearLayout2).setVisibility(View.GONE);
        findViewById(R.id.LinearLayout3).setVisibility(View.GONE);
        findViewById(R.id.LinearLayout4).setVisibility(View.GONE);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == btnPlus.getId()){
            int s = Integer.parseInt(txtCountBuy.getText().toString());
            s++;
            txtCountBuy.setText(s+"");
        } if (id == btnMines.getId()){

            int s = Integer.parseInt(txtCountBuy.getText().toString());
            if (s!=0){
                s--;
            }

            txtCountBuy.setText(s+"");
        }
        if (id== btnBuyProduct.getId()){
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            LayoutInflater inflater = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view1 = inflater.inflate(R.layout.buy_layout_dialog, null);
            alertDialogBuilder.setView(view1);
            //alertDialogBuilder.setCancelable(false);
            final AlertDialog dialog = alertDialogBuilder.create();
            dialog.show();
            view1.findViewById(R.id.btn_buy_product).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    startActivity(new Intent(MainActivity.this,FactorActivity.class));

                }
            });
        }
    }
}
