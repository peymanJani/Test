package com.app.best.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.View;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

public class SurveyActivity extends AppCompatActivity {

    public DiscreteScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);


        RecyclerSurvetAdapter aapter = new RecyclerSurvetAdapter(this);

        scrollView = findViewById(R.id.scroll_recycler_survey);
        scrollView.setAdapter(aapter);

        scrollView.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.0f)
                .setMinScale(0.95f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());


        scrollView.getLayoutManager().scrollToPosition(5);
        scrollView.scrollToPosition(scrollView.getCurrentItem() + 1);

        //
    }
}
