package com.app.best.test;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RecyclerSurvetAdapter extends RecyclerView.Adapter<RecyclerSurvetAdapter.myHolder>      {
    public RecyclerSurvetAdapter(Context context) {
        this.context = context;
    }

    Context context;
    @NonNull
    @Override
    public RecyclerSurvetAdapter.myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_survet_layout,parent,false);

        return new myHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerSurvetAdapter.myHolder holder, final int position) {
        if (position==4){
            holder.button.setVisibility(View.VISIBLE);
        }else {
            holder.button.setVisibility(View.INVISIBLE);
        }
        holder.textView.setText((position+1)+"");
        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(position!=4){
                    ((SurveyActivity)context).scrollView.smoothScrollToPosition(position+1);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return 5;
    }
    public class myHolder extends RecyclerView.ViewHolder{
        Button button;
        TextView textView;
        RadioGroup radioGroup;
        public myHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_recycler_survet_page_count);
            button = itemView.findViewById(R.id.btn_recycler_survet_layout);
            radioGroup = itemView.findViewById(R.id.radio_group_recycler_survet);
        }
    }
}
