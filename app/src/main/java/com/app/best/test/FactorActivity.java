package com.app.best.test;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

public class FactorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factor);

        new StyleableToast
                .Builder(this)
                .text("  ممنون از خرید شما   ")
                .textColor(Color.WHITE)
                .backgroundColor(Color.parseColor("#00a152"))
                .show();
        RecyclerView recyclerView =(RecyclerView) findViewById(R.id.factor_recycler);
        recyclerView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_INSET);
        recyclerView.setAdapter(new RecyclerFactorAdapter(this));

        findViewById(R.id.btn_factor_get_factor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        FactorActivity.this);
                LayoutInflater inflater=(LayoutInflater)FactorActivity.this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view1=inflater.inflate(R.layout.dialog_show_transfer,null);
                alertDialogBuilder.setView(view1);
//alertDialogBuilder.setCancelable(false);
                AlertDialog dialog=alertDialogBuilder.create();

                dialog.show();
                dialog.getWindow().setLayout(500, 650);
            }
        });
    }
}
