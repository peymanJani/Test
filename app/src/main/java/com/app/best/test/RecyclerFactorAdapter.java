package com.app.best.test;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RecyclerFactorAdapter extends RecyclerView.Adapter<RecyclerFactorAdapter.myHolder> {
    Context context;

    public RecyclerFactorAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerFactorAdapter.myHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.factor_recycler_layout,parent,false);
        return new myHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerFactorAdapter.myHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class myHolder extends RecyclerView.ViewHolder {
        public myHolder(View itemView) {
            super(itemView);
        }
    }
}
