package com.app.best.test.ProductPackage;

class ProductPojoClass {
    private int HighestPrice;
    private int LowestPrice;
    private String imgUrl;
    private float star;
    private String productName;
   private int Discount;

    public int getHighestPrice() {
        return HighestPrice;
    }

    public ProductPojoClass setHighestPrice(int highestPrice) {
        HighestPrice = highestPrice;
        return this;
    }

    public int getLowestPrice() {
        return LowestPrice;
    }

    public ProductPojoClass setLowestPrice(int lowestPrice) {
        LowestPrice = lowestPrice;
        return this;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public ProductPojoClass setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public float getStar() {
        return star;
    }

    public ProductPojoClass setStar(float star) {
        this.star = star;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public ProductPojoClass setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public int getDiscount() {
        return Discount;
    }

    public ProductPojoClass setDiscount(int discount) {
        Discount = discount;
        return this;
    }
}
