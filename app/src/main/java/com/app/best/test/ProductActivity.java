package com.app.best.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.app.best.test.ProductPackage.ProductGrideViewAdapter;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class ProductActivity extends AppCompatActivity {

    String[] img = {"https://kasbokarnews.ir/wp-content/uploads/2018/06/20180614102203.jpg","http://alavi.ir/ab/wp-content/uploads/sites/7/2016/08/006.jpg"};
    CarouselView carouselView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        final GridView gridView = (GridView) findViewById(R.id.grid_product);
        gridView.setAdapter(new ProductGrideViewAdapter(this));
        //gridView.setFastScrollAlwaysVisible(true);
        gridView.setFastScrollEnabled(true);
        gridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                gridView.requestDisallowInterceptTouchEvent(true);

                int action = motionEvent.getActionMasked();

                switch (action) {
                    case MotionEvent.ACTION_UP:
                        gridView.requestDisallowInterceptTouchEvent(false);
                        break;
                }


                return false;
            }
        });

        setCarsoulView();


    }

    private void setCarsoulView() {
        carouselView = (CarouselView) findViewById(R.id.product_carouselView);
        carouselView.setPageCount(img.length);

        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                Picasso.get().load(img[position]).fit().centerCrop().into(imageView);
            }
        });
    }
}
